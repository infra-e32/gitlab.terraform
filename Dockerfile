FROM hashicorp/terraform:1.4.5

RUN \
    apk add --no-cache \
        bash \
        git \
        go \
        jq \
        make \
        python3 \
        py-pip \
        zip \
        curl \
    && pip3 install awscli \
    && go install github.com/aquasecurity/tfsec/cmd/tfsec@latest

ENTRYPOINT []
CMD [ "/bin/sh" ]